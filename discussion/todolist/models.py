from django.db import models

# Create your models here.
class ToDoItem(models.Model):
    # CharField = String value
    # DateTimeField = datetime datatype
    task_name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    status = models.CharField(max_length = 50, default = "Pending")
    date_created = models.DateTimeField("Date Created")
