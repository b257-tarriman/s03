from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
# Import built-in 'User' model to be able to do operations to the built-in Users table from Django.
from django.contrib.auth.models import User
from .models import ToDoItem

# Create your views here.
def index(req):
    todoitem_list = ToDoItem.objects.all()
    # template = loader.get_template('todolist/index.html')
    context = {
        'todoitem_list': todoitem_list,
        'user':req.user
    }
    # output = ','.join([todoitem.task_name for todoitem in todoitem_list])
    # return HttpResponse(template.render(context,req))
    return render(req,"todolist/index.html",context)

def todoitem(req,todoitem_id):
    # model_to_dict function translates data model into a python dictionary(object). This is so that we can use the data as a regular object/dictionary.
    todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    
    return render(req,"todolist/todoitem.html",todoitem)
    # ACTIVITY
    # Create a todoitem template
    # In the template, show all the task_name ,description and status of the todoitem that is passed to it.
    # At the end of the template, create an anchor tag that will return the user to the 'index' page
def checkuser():
    users = User.objects.all()
    is_user_registered = False
    # Loops thru each existing user and checks if they already exist in the table
    for individual_user in users:
        if individual_user.username == "johndoe":
            is_user_registered = True
            break
    
    return is_user_registered

def register(req):
    is_user_registered = checkuser()
    context = {
        'is_user_registered':is_user_registered
    }
    if is_user_registered == False:
        # Creating a new instance of the User model.
        user = User()

        # Assign data values to each property of the model.
        user.username ="johndoe"
        user.first_name = "John"
        user.last_name = "Doe"
        user.email = "john@mail.com"
        user.set_password("john1234")
        user.is_staff = False
        user.is_active = True

        # Save the new instance of a user into the database.
        user.save()

        # The context will be the data to be passed to the template
        context = {
            'first_name':user.first_name,
            'last_name': user.last_name
        }

    return render(req,"todolist/register.html",context)


def change_password(req):

    is_user_authenticated = False

    user = authenticate(username="johndoe", password="john1234")
    print(user)
    if user is not None:
        authenticated_user = User.objects.get(username="johndoe")

        authenticated_user.set_password("johndoe1")
        authenticated_user.save()

        is_user_authenticated = True

    context = {
        "is_user_authenticated": is_user_authenticated
    }
    return render(req,"todolist/change_password.html",context)


def login_view(req):
    username="johndoe"
    password="johndoe1"

    user = authenticate(username=username,password=password)

    context = {
        "is_user_authenticated": False
    }

    if user is not None: 
        # The 'login' function saves the user's ID in Django's session. The request will be the one that handles the user's data and we can access the user by using 'request.user'
        login(req,user)
        return redirect("index")
    else:
        return render(req,"todolist/login.html",context)

def logout_view(req):
    logout(req)
    return redirect("index")